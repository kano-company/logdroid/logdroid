pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '25', artifactNumToKeepStr: '50'))
	}
	tools {
		gradle 'gradle-7.2'
	}
	environment {
		GRADLE_OPTS='-Dorg.gradle.daemon=false'
	}
	stages {
		stage('build') {
			when {
				not { tag '' }
			}
			steps {
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword')
				]) {
					sh 'gradle clean assemble'
				}
			}
		}
		stage('release') {
			when {
				beforeInput true
				branch 'master'
				not { tag '' }
			}
			input {
				message 'Release?'
			}
			steps {
				sh 'git config user.name cicd.agent'
				sh 'git config user.email cicd.agent'
				sh 'git config --replace-all credential.helper \'!credentialHelper() { echo username=${GIT_USERNAME}; echo password=${GIT_PASSWORD}; }; credentialHelper\''
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD'),
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword')
				]) {
					sh 'gradle publishAndTag -Prelease'
				}
			}
			post {
				always {
					sh 'git config --unset credential.helper'
				}
			}
		}
	}
}
