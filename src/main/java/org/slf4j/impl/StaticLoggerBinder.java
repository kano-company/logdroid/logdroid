package org.slf4j.impl;

import company.kano.logdroid.LogdroidLoggerFactory;

import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

public class StaticLoggerBinder implements LoggerFactoryBinder {

	private static final StaticLoggerBinder SINGLETON = new StaticLoggerBinder();

	public static StaticLoggerBinder getSingleton() {
		return SINGLETON;
	}

	private static final String loggerFactoryClassStr = LogdroidLoggerFactory.class.getName();

	private final ILoggerFactory loggerFactory;

	private StaticLoggerBinder() {
		loggerFactory = new LogdroidLoggerFactory();
	}

	public ILoggerFactory getLoggerFactory() {
		return loggerFactory;
	}

	public String getLoggerFactoryClassStr() {
		return loggerFactoryClassStr;
	}
}
