package company.kano.logdroid;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

public class LogdroidLoggerFactory implements ILoggerFactory {

	private final ConcurrentMap<String, Logger> loggerMap = new ConcurrentHashMap<>();

	public LogdroidLoggerFactory() {
		LogdroidLogger.lazyInit();
	}

	public Logger getLogger(String name) {
		Logger logger = loggerMap.get(name);
		if (logger == null) {
			Logger newInstance = new LogdroidLogger(name);
			Logger oldInstance = loggerMap.putIfAbsent(name, newInstance);
			logger = oldInstance == null ? newInstance : oldInstance;
		}
		return logger;
	}

}
