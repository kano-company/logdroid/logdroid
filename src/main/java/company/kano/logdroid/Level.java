package company.kano.logdroid;

import android.util.Log;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.slf4j.event.EventConstants;

@AllArgsConstructor
@Getter
public enum Level {

	TRACE(EventConstants.TRACE_INT, Log.VERBOSE, "VERBOSE"),
	DEBUG(EventConstants.DEBUG_INT, Log.DEBUG, "DEBUG"),
	INFO(EventConstants.INFO_INT, Log.INFO, "INFO"),
	WARN(EventConstants.WARN_INT, Log.WARN, "WARN"),
	ERROR(EventConstants.ERROR_INT, Log.ERROR, "ERROR"),
	OFF(EventConstants.ERROR_INT+10, Log.ASSERT+1, "OFF");

	private final int slf4jInt;
	private final int androidInt;
	private final String androidName;

}
