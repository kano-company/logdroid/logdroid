package company.kano.logdroid;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import lombok.Getter;

@Getter
public class LogdroidLoggerConfiguration {

	private Level level = Level.INFO;

	private int maxFile = 2;

	private String applicationId;
	private String applicationVersion;
	private Integer applicationBuildNumber;

	private String osVersion = "Android " + Build.VERSION.RELEASE + " (API " + Build.VERSION.SDK_INT + ")";
	private String osBuildNumber = Build.DISPLAY;

	private String deviceModel = Build.BRAND + " - " + Build.MODEL + " - " + Build.DEVICE + " - " + Build.PRODUCT;

	void init() {
		Context context = AndroidUtils.getContext();
		if (context == null) {
			return;
		}

		applicationId = context.getPackageName();
		try {
			PackageInfo pkgInfo = context.getPackageManager().getPackageInfo(applicationId, 0);
			applicationVersion = pkgInfo.versionName;
			applicationBuildNumber = pkgInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			Log.w(LogdroidLogger.TAG, "Error while retrieving package information. " + e.getMessage(), e);
		}

		try (InputStream assetInputStream = context.getAssets().open("logdroid.properties")) {
			loadProperties(assetInputStream);
		} catch (IOException e) {
			Log.w(LogdroidLogger.TAG, "Error while reading the configuration from assets. " + e.getMessage(), e);
		}

		try (InputStream fileInputStream = context.openFileInput("logdroid.properties")) {
			loadProperties(fileInputStream);
		} catch (IOException e) {
			Log.w(LogdroidLogger.TAG, "Error while reading the configuration from files. " + e.getMessage(), e);
		}
	}

	private void loadProperties(InputStream in) throws IOException {
		Properties properties = new Properties();
		properties.load(in);

		try {
			String levelString = properties.getProperty("logdroid.level");
			if (levelString != null) {
				level = Level.valueOf(levelString);
			}
		} catch (IllegalArgumentException e) {
			Log.w(LogdroidLogger.TAG, "logdroid.level error: " + e.getMessage(), e);
		}

		try {
			String maxFileString = properties.getProperty("logdroid.maxFile");
			if (maxFileString != null) {
				int maxFileInt = Integer.valueOf(maxFileString);
				if (maxFileInt < 0) {
					throw new IllegalArgumentException("Must be positive");
				}
				maxFile = maxFileInt;
			}
		} catch (IllegalArgumentException e) {
			Log.w(LogdroidLogger.TAG, "logdroid.maxFile error: " + e.getMessage(), e);
		}
	}

}
