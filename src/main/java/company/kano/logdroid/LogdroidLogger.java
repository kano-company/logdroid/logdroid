package company.kano.logdroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import android.content.Context;
import android.util.Log;

import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;

public class LogdroidLogger extends MarkerIgnoringBase {

	public static final String TAG = "LogDroid";

	private final String tag;

	private Level currentLogLevel;

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

	private static boolean INITIALIZED = false;

	private static final LogdroidLoggerConfiguration CONFIG_PARAMS = new LogdroidLoggerConfiguration();

	/**
	 * Package access allows only {@link LogdroidLoggerFactory} to instantiate LogdroidLogger instances.
	 */
	LogdroidLogger(String name) {
		this.name = name;
		this.tag = getTag(name);

		this.currentLogLevel = CONFIG_PARAMS.getLevel();
		AndroidUtils.setProp("log.tag." + this.tag, this.currentLogLevel.getAndroidName());
	}

	static void lazyInit() {
		if (INITIALIZED) {
			return;
		}
		INITIALIZED = true;
		init();
	}

	// external software might be invoking this method directly. Do not rename or change its semantics.
	static void init() {
		CONFIG_PARAMS.init();
	}

	@Override
	public boolean isTraceEnabled() {
		return isLevelEnabled(Level.TRACE);
	}

	@Override
	public void trace(String msg) {
		log(Level.TRACE, msg, null);
	}

	@Override
	public void trace(String format, Object arg) {
		formatAndLog(Level.TRACE, format, arg);
	}

	@Override
	public void trace(String format, Object arg1, Object arg2) {
		formatAndLog(Level.TRACE, format, arg1, arg2);
	}

	@Override
	public void trace(String format, Object... arguments) {
		formatAndLog(Level.TRACE, format, arguments);
	}

	@Override
	public void trace(String msg, Throwable t) {
		log(Level.TRACE, msg, t);
	}

	@Override
	public boolean isDebugEnabled() {
		return isLevelEnabled(Level.DEBUG);
	}

	@Override
	public void debug(String msg) {
		log(Level.DEBUG, msg, null);
	}

	@Override
	public void debug(String format, Object arg) {
		formatAndLog(Level.DEBUG, format, arg);
	}

	@Override
	public void debug(String format, Object arg1, Object arg2) {
		formatAndLog(Level.DEBUG, format, arg1, arg2);
	}

	@Override
	public void debug(String format, Object... arguments) {
		formatAndLog(Level.DEBUG, format, arguments);
	}

	@Override
	public void debug(String msg, Throwable t) {
		log(Level.DEBUG, msg, t);
	}

	@Override
	public boolean isInfoEnabled() {
		return isLevelEnabled(Level.INFO);
	}

	@Override
	public void info(String msg) {
		log(Level.INFO, msg, null);
	}

	@Override
	public void info(String format, Object arg) {
		formatAndLog(Level.INFO, format, arg);
	}

	@Override
	public void info(String format, Object arg1, Object arg2) {
		formatAndLog(Level.INFO, format, arg1, arg2);
	}

	@Override
	public void info(String format, Object... arguments) {
		formatAndLog(Level.INFO, format, arguments);
	}

	@Override
	public void info(String msg, Throwable t) {
		log(Level.INFO, msg, t);
	}

	@Override
	public boolean isWarnEnabled() {
		return isLevelEnabled(Level.WARN);
	}

	@Override
	public void warn(String msg) {
		log(Level.WARN, msg, null);
	}

	@Override
	public void warn(String format, Object arg) {
		formatAndLog(Level.WARN, format, arg);
	}

	@Override
	public void warn(String format, Object arg1, Object arg2) {
		formatAndLog(Level.WARN, format, arg1, arg2);
	}

	@Override
	public void warn(String format, Object... arguments) {
		formatAndLog(Level.WARN, format, arguments);
	}

	@Override
	public void warn(String msg, Throwable t) {
		log(Level.WARN, msg, t);
	}

	@Override
	public boolean isErrorEnabled() {
		return isLevelEnabled(Level.ERROR);
	}

	@Override
	public void error(String msg) {
		log(Level.ERROR, msg, null);
	}

	@Override
	public void error(String format, Object arg) {
		formatAndLog(Level.ERROR, format, arg);
	}

	@Override
	public void error(String format, Object arg1, Object arg2) {
		formatAndLog(Level.ERROR, format, arg1, arg2);
	}

	@Override
	public void error(String format, Object... arguments) {
		formatAndLog(Level.ERROR, format, arguments);
	}

	@Override
	public void error(String msg, Throwable t) {
		log(Level.ERROR, msg, t);
	}

	protected boolean isLevelEnabled(Level logLevel) {
		return (logLevel.getSlf4jInt() >= currentLogLevel.getSlf4jInt());
	}

	private void formatAndLog(Level level, String format, Object... argArray) {
		if (!isLevelEnabled(level)) {
			return;
		}
		FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
		logInternal(level, ft.getMessage(), ft.getThrowable());
	}

	private void log(Level level, String message, Throwable throwable) {
		if (!isLevelEnabled(level)) {
			return;
		}
		logInternal(level, message, throwable);
	}

	private void logInternal(Level level, String message, Throwable throwable) {
		if (throwable != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			throwable.printStackTrace(pw);
			message += '\n' + sw.toString();
		}

		Log.println(level.getAndroidInt(), tag, message);

		Context context = AndroidUtils.getContext();
		if (context != null) {
			File logDir = context.getDir("log", Context.MODE_PRIVATE); // creating if needed
			if (CONFIG_PARAMS.getMaxFile() > 0) {
				Date now = new Date();
				try (FileOutputStream fileOutputStream = new FileOutputStream(new File(logDir, CONFIG_PARAMS.getApplicationId() + "_" + dateFormat.format(now) + ".log"), true)) {
					//TODO org.apache.commons:commons-text StringSubstitutor
					StringBuilder sb = new StringBuilder()
							.append(dateTimeFormat.format(now))
							.append(" | ")
							.append(level.name())
							.append(" | ")
							.append(CONFIG_PARAMS.getApplicationId())
							.append(" | ")
							.append(CONFIG_PARAMS.getApplicationVersion())
							.append(" | ")
							.append(CONFIG_PARAMS.getOsVersion())
							.append(" | ")
							.append(CONFIG_PARAMS.getOsBuildNumber())
							.append(" | ")
							.append(CONFIG_PARAMS.getDeviceModel())
							.append(" | ")
							.append(name)
							.append(" | ")
							.append(Thread.currentThread().getId()).append(" - ").append(Thread.currentThread().getName())
							.append(" | ")
							.append(message)
							.append('\n');
					fileOutputStream.write(sb.toString().getBytes("UTF-8")); // StandardCharsets.UTF_8 -> added in API level 19
				} catch (IOException e) {
					Log.w(LogdroidLogger.TAG, "Error while writing to the log file. " + e.getMessage(), e);
				}
			}
			// delete old log
			File[] fileArray = logDir.listFiles();
			if (fileArray != null && fileArray.length > CONFIG_PARAMS.getMaxFile()) {
				Arrays.sort(fileArray, new Comparator<File>() {
					@Override
					public int compare(File file1, File file2) {
						// desc order
						return file2.getName().compareTo(file1.getName());
					}
				});
				for (int i = fileArray.length-1 ; i >= CONFIG_PARAMS.getMaxFile() ; i--) {
					fileArray[i].delete();
				}
			}
		}
	}

	private String getTag(String name) {
		String tag = name;
		if (tag.length() > 23) {
			tag = "-" + tag.substring(tag.length() - 22);
		}
		return tag;
	}

}
