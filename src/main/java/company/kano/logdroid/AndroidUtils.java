package company.kano.logdroid;

import java.lang.reflect.Method;

import android.content.ContextWrapper;
import android.util.Log;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AndroidUtils {

	public static ContextWrapper getContext() {
		try {
			Class<?> c = Class.forName("android.app.AppGlobals");
			Method method = c.getDeclaredMethod("getInitialApplication");
			return (ContextWrapper) method.invoke(c);
		} catch (Throwable t) {
			Log.e(LogdroidLogger.TAG, "Error while retrieving context. " + t.getMessage(), t);
		}
		return null;
	}

	public static void setProp(String key, String value) {
		try {
			// android.os.SystemProperties.set(key, value)
			Runtime.getRuntime().exec(new String[]{"setprop", key, value});
		} catch (Throwable t) {
			Log.e(LogdroidLogger.TAG, "Error during the call to setprop. " + t.getMessage(), t);
		}
	}

}
