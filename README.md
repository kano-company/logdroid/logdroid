# Logdroid

Logdroid est un framework de journalisation (logging) pour android implémentant SLF4J.
Les journaux sont enregistrés dans logcat et dans un fichier.

## Configuration

Logdroid est configurable via un fichier `logdroid.properties` placé dans le répertoire assets et/ou dans le répertoire de l'application `getFilesDir()`.

```
# niveau de journalisation (TRACE, DEBUG, INFO, WARN, ERROR, OFF)
# valeur par défaut : INFO
logdroid.level=INFO
# nombre de fichiers de journalisation
# valeur par défaut : 2
logdroid.maxFile=2
```

## Licence

Logdroid est distribué sous licence [Apache v2.0](https://www.apache.org/licenses/LICENSE-2.0).
